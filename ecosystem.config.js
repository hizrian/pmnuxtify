module.exports = {
  apps: [
    {
      name: 'petitemauree-front',
      port: 3333,
      watch: true,
      script: './node_modules/nuxt/bin/nuxt.js',
      args: 'start'
    }
  ]
}
