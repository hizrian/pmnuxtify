export default function ({
  $axios,
  redirect,
  app
}) {

  $axios.onError(async error => {
    const code = parseInt(error.response && error.response.status)
    if (code === 401 && error.response.data.message === 'Token Invalid') {
      try {
        const response = await app.$axios.$post('/accounts/refresh-token', {}, {
          withCredentials: true
        })
        app.$auth.setUserToken(response.jwtToken)
      } catch (err) {
        app.$auth.logout()
        redirect('/auth/login')
      }
    } else {
      return Promise.reject(error)
    }
  })
}
