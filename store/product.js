export const state = () => ({
  products: []
})

export const mutations = {
  setProducts(state, products) {
    state.products = products
  }
}

export const actions = {
  async getProducts({commit}) {
    this.$axios.$get('/products/all').then((res) => {
        commit('setProducts', res)
    })
  }
}

export const getters = {
  products: state => {
    return state.products
  }
}
